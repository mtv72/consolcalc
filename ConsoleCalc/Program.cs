﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleCalc
{
    class Program
    {
        static void Main(string[] args)
        {
            string stroka = Console.ReadLine();
            stroka = stroka.Replace(".", ",");
            string err = ProvStr(stroka.Replace(".", ","));
            if (!string.IsNullOrEmpty(err))
                Console.Write(string.Format("Пример содержит ошибку! {0} Нажмите любую кнопку для завершения.",err));
            else
            {
                string Value = CalcPrimer(stroka);
                Console.Write("Значение = " + Value);
                Console.Write(" Нажмите любую кнопку для завершения.");
            }
            Console.ReadKey(true);

            //Console.Write("Press any key to continue...");
            //Console.ReadKey(true);
        }
        static string CalcPrimer(string str)
        {
            decimal val = 0;
            char[] arr = str.ToCharArray();
            List<string> newStr = new List<string>();
            List<char> ch = new List<char>();
            string tekNum = "";
            //собираем лист с числами и операциями
            for (int i = 0; i < str.Length; i++)
            {
                if (Int32.TryParse(arr[i].ToString(), out int num)|| arr[i]=='.'|| arr[i]==',')
                    tekNum +=num==0? arr[i].ToString():num.ToString();
                else
                {
                    newStr.Add(tekNum);
                    //newStr.Add(arr[i].ToString());
                    ch.Add(arr[i]);
                    tekNum = "";
                }

            }
            newStr.Add(tekNum);

            //делаем операции * и /
            List<string> ItogStr = new List<string>();
            List<char> Itogch = new List<char>();
            int k = 0;

            bool f = false;
            decimal a0 = 0;
            for (int i = 0; i < ch.Count; i++)
            {
                if (!f)
                    Decimal.TryParse(newStr[i], out a0);
                Decimal.TryParse(newStr[i + 1], out decimal a2);
                f = false;
                switch (ch[i])
                {
                    case '*':
                        a0 = a0 * a2;
                        f = true;
                        break;
                    case '/':
                        a0 = a0 / a2;
                        f = true;
                        break;
                    default:
                        Itogch.Add(ch[i]);
                        break;
                }
                if (!f)
                {
                    ItogStr.Add(a0.ToString());
                    k++;
                }

            }
            if (f)
                ItogStr.Add(a0.ToString());

            Decimal.TryParse(ItogStr[0], out a0);

            for (int i = 0; i < Itogch.Count; i++)
            {
                Decimal.TryParse(ItogStr[i + 1], out decimal a2);
                switch (Itogch[i])
                {
                    case '+':
                        a0 = a0 + a2;
                        break;
                    case '-':
                        a0 = a0 - a2;
                        break;
                }
            }
            val = a0;
            return val.ToString();

        }
        //проверка правильности ввода примера
        static string ProvStr(string str)
        {
            bool f = true, operF = false;
            string err = "";
            char[] arr = str.ToCharArray();

            for (int i = 0; i < str.Length; i++)
            {
                if (arr[i] == '*' || arr[i] == '/' || arr[i] == '+' || arr[i] == '-')
                {
                    if (operF || i == 0)
                    {
                        f = false;
                        err = "Не может начинаться с операции!";
                        break;
                    }
                    if (arr[i] == '/' && arr[i + 1] == '0')
                    {
                        err = "Деление на 0!";
                        break;
                    }
                    else operF = true;
                }
                else
                {
                    f = Int32.TryParse(arr[i].ToString(), out int num);
                    if (f || (arr[i] == ',' && !operF))
                    {
                        operF = false;
                    }
                    else
                    {
                        err = "Только цифра!";
                        break;
                    }
                }
            }

            return err;
        }
    }
}
